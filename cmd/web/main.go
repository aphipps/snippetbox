package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
}

func main() {
	// Command line flags. Default value for addr=":4000"
	addr := flag.String("addr", ":4000", "HTTP network address")
	flag.Parse()

	// Stdout logging
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)

	// Stderror logging
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	app := &application{
		errorLog: errorLog,
		infoLog:  infoLog,
	}

	svr := &http.Server{
		Addr:     *addr,
		ErrorLog: errorLog,
		Handler:  app.routes(),
	}

	infoLog.Printf("Starting server on %s", *addr)
	err := svr.ListenAndServe()
	errorLog.Fatal(err)
}

# Let's Go: Snippetbox project

## Overview
This project is a walk through of the book [Let's Go](https://lets-go.alexedwards.net) book by Alex Edwards.

## Notes

### Chapter 3 Confirguration and Error Handling

#### Chapter 3.1 Manaing confirguration settings

- How to use command line arguments. e.g. To run this application on port 9999 you can use this command `go run ./cmd/web -addr=":9999"`
- Type converstion for command line arguments can be done by using `flag.Int()`, `flag.bool()` and `flag.Float64()`.
- Automated help for command line aguements is generated automatically. e.g. run `go run ./cmd/web -help`

#### Chapter 3.2 Leveled Logging

- Go allows you to create custom loggers.
- In this example the two new loggers we created were `errorLog()` and `inforLog()`, they output to `stdout` and `stderror`.
- You can then route these outputs to log files by redirecting them like so `go run ./cmd/web >>/tmp/info.log 2>>/tmp/error.log`
- A struct of type http.Server was created to define the server port, error logging and handler to simplify and define error logging.